{
    
    "What is your name?" input "player" store
    "Lain" "Hello <player>" .

    `
    affection = 0.5
    ` python

    { "Lain" "I really like you <player>" . }
    { "Lain" "I like you less" . }
    "stack.append(affection > 0.6)" python
    switch

    `
    affection += 0.2
    `

    { "Lain" "I really like you <player>" . }
    { "Lain" "I like you less" . }
    switch

} "start" block

"start" jump
