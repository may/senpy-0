{
    "NARRATOR" "You arrive at the restaurant and take your seat with your date." .
    "Alice" "Hey <player>! Are you ready for an amazing dinner?" .
    { "Yes" "No, I'm not feeling hungry" } menu get
    { "back_out" jump } |
    { "order" jump } |
    select
} "start_block" block

{
    "Alice" "Told you it was going to be great! Let's take a look at today's specials" . 
    "Alice" "So, what are you going to choose?" .
    { "Steak" "Salad" "Nothing" } menu get
    { "back_out" jump } |
    { "order_steak" jump } |
    { "order_salad" jump } |
    select
} "order" block 

{ 
    "Alice" "Uh oh, are you not feeling ok? Is there something I can do to help?" . 
    { "Yes" "No" } menu get 
    { "help" jump } |
    { "no_help" jump } |
    select
} "back_out" block

{
    "Alice" "Ah, a classic steak. I like it!" . 
    "Alice" "Would you like anything to drink, or is a glass of water enough?" .
    { "Yes" "No" } menu get 
    { "drinks" jump } |
    { "no_drinks" jump } |
    select
} "order_steak" block 

{
    "Alice" "Ah, a healthy option. Smart choice!" .
    "Alice" "Would you like anything to drink, or is a glass of water enough?" .
    { "Yes" "No" } menu get 
    { "drinks" jump } |
    { "no_drinks" jump } |
    select
} "order_salad" block 

{
    "Alice" "Oh, that's too bad. Is there anything I can do to help you feel better?" .
    { "Yes" "No" } menu get 
    { "help" jump } |
    { "no_help" jump } |
    select
} "no_help" block 

{
    "Alice" "Of course, just let me know what I can do." .
    { "I am feeling better" "I still don't feel good" } menu get
    { "feeling_better" jump } |
    { "not_feeling_better" jump } |
    select
} "help" block 

{
    "Alice" "Ok, I'm glad you are feeling better. What would you like to drink?" .
    { "Beer" "Cola" "Water" } menu get
    { "order_beer" jump } |
    { "order_cola" jump } |
    { "order_water" jump } |
    select
} "drinks" block 

{
    "Alice" "Alright then, let's just order the food and move on" . 
    "Alice" "Let me know when you are ready for the food" . 
    { "Ready for food" } menu get
    { "food_ready" jump } |
    select
} "no_drinks" block 

{
    "Alice" "Alright then let's order some beer" . 
    "Alice" "Let me know when you are ready for the food" . 
    { "Ready for food" } menu get
    { "food_ready" jump } |
    select
} "order_beer" block 

{
    "Alice" "Alright then let's order some cola" . 
    "Alice" "Let me know when you are ready for the food" . 
    { "Ready for food" } menu get
    { "food_ready" jump } |
    select
} "order_cola" block 

{
    "Alice" "Alright then let's have a glass of water" . 
    "Alice" "Let me know when you are ready for the food" . 
    { "Ready for food" } menu get
    { "food_ready" jump } |
    select
} "order_water" block

{
    "Alice" "Ok, the food will be here soon" . 
    "Bob" "So, what have you been up to lately?" .
} "food_ready" block 

{
    "Alice" "Okay, no problem. Feel better soon!" .
} "not_feeling_better" block 

{
    "Alice" "That's great! Let me know what I can do to help." . 
} "feeling_better" block 

"start_block" jump
