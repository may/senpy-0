from typing import Any, List
from enum import Enum
from dataclasses import dataclass
import compiler

class EventType(Enum):
    Text = 1
    Menu = 2
    MenuResponse = 3
    Confirm = 4

@dataclass
class Event:
    et: EventType
    data: Any

class Runtime:
    def __init__(self, ast: List[compiler.Node]):
        self.ast = ast
        self.pc = 0
        self.stack = []
        self.options = []
        self.blocks = {}
        self.locals = {"self": self}
        self.call_stack = []

    def jump_to(self, block):
        self.call_stack.append((self.ast, self.pc))
        self.ast = block
        self.pc = -1

    def execute_operator(self, op: str, data: Event):
        if op == 'block':
            name = self.stack.pop().data
            block = self.stack.pop().data
            self.blocks[name] = block
            return (False, None)

        elif op == 'jump':
            name = self.stack.pop().data
            block = self.blocks[name]
            self.jump_to(block)
            return (False, None)

        elif op == '.':
            text = self.stack.pop()
            speaker = self.stack.pop()
            if isinstance(text, compiler.String):
                return (True, Event(EventType.Text, [speaker.data, str(text.data)]))
            else:
                block = text.data
                new_runtime = Runtime(block)
                new_runtime.locals = self.locals
                p = ""
                while p is not None:
                    p = new_runtime.poll(Event(EventType.Confirm, []))
                result = new_runtime.stack
                return (True, Event(EventType.Text, [speaker.data] + [str(line.data) for line in result]))

        elif op == 'menu':
            options = self.stack.pop().data
            self.options = []
            return (True, Event(EventType.Menu, [opt.data for opt in options]))

        elif op == 'get':
            self.stack.append(int(data.data))
            return (False, None)

        elif op == '|':
            action = self.stack.pop()
            self.options.append(action)
            return (False, None)

        elif op == 'select':
            index = self.stack.pop()
            option = self.options[index]
            self.jump_to(option.data)
            return (False, None)

        elif op == 'python':
            code = self.stack.pop().data
            white_space = 0
            for char in code:
                if char in compiler.WHITESPACE:
                    white_space += 1
                else:
                    break
            lines = code.split('\n')
            lines = [line[white_space-1:] for line in lines]
            code = '\n'.join(lines)
            exec(code, globals(), self.locals)
            return (False, None)

        elif op == '$':
            key = self.stack.pop().data
            value = self.locals[key]
            self.stack.append(compiler.String(value))
            return (False, None)

        elif op == '#':
            self.stack.pop()
            return (False, None)

        print("Unknown operator:")
        print(op)
        return (False, None)

    def execute_instruction(self, instruction: compiler.Node, event):
        if isinstance(instruction, compiler.Block):
            self.stack.append(instruction)
            return (False, None)
        elif isinstance(instruction, compiler.String):
            self.stack.append(instruction)
            return (False, None)
        elif isinstance(instruction, compiler.Operator):
            op = instruction.data
            return self.execute_operator(op, event)
        else:
            print("???? how did you do this")
            exit(100000000000)

    def poll(self, event):
        done_polling = False
        data = None
        while not done_polling:
            for _ in range(len(self.call_stack) + 1):
                if self.pc == len(self.ast):
                    if len(self.call_stack) > 0:
                        (ast, pc) = self.call_stack.pop()
                        self.ast = ast
                        self.pc = pc + 1
                    else:
                        return None
            instruction = self.ast[self.pc]
            (done_polling, data) = self.execute_instruction(instruction, event)
            self.pc += 1
            # execute_instruction will return True on a polling operation,
            # such as input or output
        return data

