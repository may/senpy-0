from rich.console import Console
from rich.text import Text
from datetime import datetime

def info(message):
    time = datetime.now().strftime("%H:%M:%S")
    label_text = Text("INFO", style="bold yellow")
    time_text = Text(time, style="#909090")
    message_text = Text(message, style="bold white")
    Console().print(label_text, time_text, message_text)
