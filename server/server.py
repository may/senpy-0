import compiler
from runtime import Runtime, Event, EventType
from log import info
import asyncio
from websockets.server import serve
import json

PATH = "../example-game/gpt.sps"
IP = "localhost"
PORT = 8765

async def handler(websocket):
    info("Received connection")
    info(f"Loading data from {PATH}")

    with open(PATH, 'r') as file:
        text = file.read()
    tokens = compiler.Lexer(text).lex()
    ast = compiler.parse(tokens)
    runtime = Runtime(ast)

    async for message in websocket:
        data = json.loads(message)
        if data["event"] == "confirm":
            event = Event(EventType.Confirm, "")
        elif data["event"] == "menu_response":
            event = Event(EventType.MenuResponse, int(data["data"][0]))
        else:
            print("UNKNOWN EVENT")
            print(data)
            event = None
        response = runtime.poll(event)
        if response is not None:
            if response.et == EventType.Text:
                event = {
                    "event": "text",
                    "data": response.data
                }
            elif response.et == EventType.Menu:
                event = {
                    "event": "menu",
                    "data": response.data
                }
            await websocket.send(json.dumps(event))
        else:
            await websocket.send(json.dumps({
                "event": "closure",
                "data": []
            }))
            info("End reached, connection closing")
            return


async def main():
    info(f"Serving on {IP}:{PORT}")
    async with serve(handler, IP, PORT):
        await asyncio.Future()

asyncio.run(main())
