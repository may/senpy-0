from enum import Enum
from dataclasses import dataclass
from typing import List

WHITESPACE = ' \t\n'

class TT(Enum):
    String = 0
    Operator = 1
    OpenBracket = 2
    CloseBracket = 3
    Literal = 4

@dataclass
class Token:
    tt: TT
    value: str

class Lexer:
    def __init__(self, text: str):
        self.text = text + '\n'
        self.i = -1
        self.advance()

    def advance(self):
        self.i += 1
        self.char = self.text[self.i]

    def make_string(self, delim: str):
        result = ""
        self.advance()
        while self.char != delim:
            result += self.char
            self.advance()
        return result

    def make_operator(self):
        result = ""
        while self.char not in WHITESPACE:
            result += self.char
            self.advance()
        return result

    def lex(self):
        tokens = []
        while self.i < len(self.text)-1:

            if self.char in WHITESPACE:
                pass
            elif self.char == '"':
                text = self.make_string('"')
                tokens.append(Token(TT.String, text))
            elif self.char == '`':
                text = self.make_string('`')
                tokens.append(Token(TT.Literal, text))
            elif self.char == '{':
                tokens.append(Token(TT.OpenBracket, ""))
            elif self.char == '}':
                tokens.append(Token(TT.CloseBracket, ""))
            else:
                text = self.make_operator()
                tokens.append(Token(TT.Operator, text))

            self.advance()
        return tokens

class Node:
    data = ""

@dataclass
class String(Node):
    data: str

@dataclass
class Operator(Node):
    data: str

@dataclass
class Block(Node):
    data: List[Node]

def parse(tokens: List[Token]) -> List[Node]:
    result = []
    builder = []
    level = 0 # How many levels of indentation are we in
    
    for token in tokens:
        if token.tt == TT.OpenBracket:
            level += 1
            if level != 1:
                builder.append(token)

        elif token.tt == TT.CloseBracket:
            level -= 1
            if level == 0:
                inner_node = parse(builder)
                result.append(Block(inner_node))
                builder = []
            else:
                builder.append(token)

        elif level == 0:
            if token.tt in [TT.String, TT.Literal]:
                result.append(String(token.value))
            else:
                result.append(Operator(token.value))
        else:
            builder.append(token)

    return result
