use std::{
    io::{self, Write},
    net::TcpStream,
};

use serde::{Deserialize, Serialize};
use tungstenite::{connect, stream::MaybeTlsStream, Message, WebSocket};
use url::Url;

#[derive(Serialize, Deserialize, Debug)]
struct Event {
    event: String,
    data: Vec<String>,
}

fn send_event(
    socket: &mut WebSocket<MaybeTlsStream<TcpStream>>,
    event: impl ToString,
    data: Vec<String>,
) {
    socket
        .write_message(Message::Text(
            serde_json::to_string(&Event {
                event: event.to_string(),
                data,
            })
            .unwrap(),
        ))
        .unwrap();
}

fn input(prompt: &str) -> String {
    print!("{}", prompt);
    io::stdout().flush().unwrap();
    let mut temp = String::new();
    io::stdin().read_line(&mut temp).unwrap();
    temp
}

fn main() {
    let (mut socket, _) = connect(Url::parse("ws://127.0.0.1:8765").unwrap()).unwrap();
    send_event(&mut socket, "confirm", vec![]);
    'main: loop {
        let msg = socket.read_message().unwrap();
        if let Message::Text(text) = msg.clone() {
            let event: Event = serde_json::from_str(&text).unwrap();
            let mut data = event.data.iter();
            if event.event == "text".to_string() {
                let _ = input("");
                println!("=={}==", data.next().unwrap());
                for line in data {
                    println!("    {}", line);
                }
                send_event(&mut socket, "confirm", vec![]);
            } else if event.event == "menu".to_string() {
                println!();
                for (i, option) in data.enumerate() {
                    println!("{}| {}", i, option);
                }
                let response = input(" > ");
                send_event(&mut socket, "menu_response", vec![response]);
            } else if event.event == "closure".to_string() {
                socket.close(None).unwrap();
                break 'main;
            }
        } else if let Message::Ping(p) = msg {
            socket.write_message(Message::Pong(p)).unwrap();
        }
    }
}
